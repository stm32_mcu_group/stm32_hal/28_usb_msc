//
// Created by Csurleny on 8/26/2022.
//

#ifndef INC_28_USB_MSC_HOST_FILEHANDLING_H
#define INC_28_USB_MSC_HOST_FILEHANDLING_H

#include "fatfs.h"
#include <string.h>
#include <stdio.h>

void mountUSB(void);
void unmountUSB (void);
void checkUSBdetails (void);
FRESULT scanUSB (char* pat);
FRESULT formatUSB (void);
FRESULT writeFile (char *name, char *data);
FRESULT readFile (char *name);
FRESULT createFile (char *name);
FRESULT removeFile (char *name);
FRESULT createDir (char *name);
FRESULT updateFile (char *name, char *data);

#endif //INC_28_USB_MSC_HOST_FILEHANDLING_H
