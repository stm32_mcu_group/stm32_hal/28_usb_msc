//
// Created by Csurleny on 8/26/2022.
//

#include "fileHandling.h"
#include "main.h"

extern UART_HandleTypeDef huart2;
#define pUART &huart2

extern char USBHPath[4];
extern FATFS USBHFatFS;
extern FIL USBHFile;

FILINFO USBHfno;
FRESULT fresult;
UINT br, bw;

FATFS *pUSBHFatFS;
DWORD fre_clust;
uint32_t total, free_space;

int _write(int fd, char *ptr, int len){
    HAL_UART_Transmit(pUART, (uint8_t *) ptr, len, HAL_MAX_DELAY);
    return len;
}

void mountUSB(void){
    fresult = f_mount(&USBHFatFS, USBHPath, 1);
    if(fresult != FR_OK)
        printf("Error in mounting USB.\r\n");
    else
        printf("USB drive mounted successfully.\r\n");
}

void unmountUSB (void){
    fresult = f_mount(NULL, USBHPath, 1);
    if(fresult != FR_OK)
        printf("Error in unmounting USB.\r\n");
    else
        printf("USB drive unmounted successfully.\r\n");
}

void checkUSBdetails (void){
    f_getfree("", &fre_clust, &pUSBHFatFS);
    total = (uint32_t)((pUSBHFatFS->n_fatent - 2) * pUSBHFatFS->csize * 0.5);
    printf("USB  Total Size: \t%lu\r\n", total);
    free_space = (uint32_t)(fre_clust * pUSBHFatFS->csize * 0.5);
    printf("USB Free Space: \t%lu\r\n", free_space);
}

FRESULT scanUSB (char* pat){
    DIR dir;
    UINT i;
    char *path = malloc(20*sizeof (char));
    sprintf (path, "%s",pat);
    fresult = f_opendir(&dir, path);

    if(fresult == FR_OK){
        for (;;){
            fresult = f_readdir(&dir, &USBHfno);
            if (fresult != FR_OK || USBHfno.fname[0] == 0)
                break;
            if (USBHfno.fattrib & AM_DIR){
                if (!(strcmp ("SYSTEM~1", USBHfno.fname)))
                    continue;
                if (!(strcmp("System Volume Information", USBHfno.fname)))
                    continue;
                printf("Dir %s\r\n", USBHfno.fname);
                i = strlen(path);
                sprintf(&path[i], "/%s", USBHfno.fname);
                fresult = scanUSB(path);
                if (fresult != FR_OK) break;
                path[i] = 0;
            }else{
                printf("File %s/%s\r\n", path, USBHfno.fname);
            }
        }
        f_closedir(&dir);
    }
    free(path);
    return fresult;
}

FRESULT formatUSB (void){
    DIR dir;
    char *path = malloc(20*sizeof (char));
    sprintf (path, "%s","/");

    fresult = f_opendir(&dir, path);
    if(fresult == FR_OK){
        for (;;){
            fresult = f_readdir(&dir, &USBHfno);
            if (fresult != FR_OK || USBHfno.fname[0] == 0)
                break;
            if (USBHfno.fattrib & AM_DIR){
                if (!(strcmp ("SYSTEM~1", USBHfno.fname)))
                    continue;
                if (!(strcmp("System Volume Information", USBHfno.fname)))
                    continue;
                fresult = f_unlink(USBHfno.fname);
                if (fresult == FR_DENIED)
                    continue;
            }else{
                fresult = f_unlink(USBHfno.fname);
            }
        }
        f_closedir(&dir);
    }
    free(path);
    return fresult;
}

FRESULT writeFile (char *name, char *data){
    fresult = f_stat (name, &USBHfno);
    if (fresult != FR_OK){
        printf("Error, %s does not exist.\r\n", name);
        return fresult;
    }else{
        fresult = f_open(&USBHFile, name, FA_OPEN_EXISTING | FA_WRITE);
        if (fresult != FR_OK){
            printf("Error No. %d in opening file %s.\r\n", fresult, name);
            return fresult;
        }else{
            printf("Opening file %s to write data into it.\r\n", name);

            fresult = f_write(&USBHFile, data, strlen(data), &bw);
            if (fresult != FR_OK){
                printf("Error No. %d while writing to the file %s.\r\n", fresult, name);
            }

            fresult = f_close(&USBHFile);
            if (fresult != FR_OK){
                printf("Error No. %d in closing file %s after writing.\r\n", fresult, name);
            }else{
                printf("Write operation successful, closed file %s.\r\n", name);
            }
        }
        return fresult;
    }
}

FRESULT readFile (char *name){
    fresult = f_stat (name, &USBHfno);
    if (fresult != FR_OK){
        printf("Error, %s does not exist.\r\n", name);
        return fresult;
    }else{

        fresult = f_open(&USBHFile, name, FA_READ);

        if (fresult != FR_OK){
            printf("Error No. %d in opening file %s.\r\n", fresult, name);
            return fresult;
        }

        printf("Opening file %s to read data from it.\r\n", name);

        char *buffer = malloc(sizeof(f_size(&USBHFile)));
        fresult = f_read (&USBHFile, buffer, f_size(&USBHFile), &br);
        if (fresult != FR_OK){
            free(buffer);
            printf("Error No. %d in reading file %s\r\n", fresult, name);
        }else{
            printf("%s\r\n", buffer);
            free(buffer);

            fresult = f_close(&USBHFile);
            if (fresult != FR_OK){
                printf("Error No. %d in closing file %s.\r\n", fresult, name);
            }else{
                printf("File %s is closed successfully.\r\n", name);
            }
        }
        return fresult;
    }
}

FRESULT createFile (char *name){

    fresult = f_stat (name, &USBHfno);
    if (fresult == FR_OK){
        printf("Error, %s already exists, use Update instead.\r\n", name);
        return fresult;
    }else{
        fresult = f_open(&USBHFile, name, FA_CREATE_ALWAYS|FA_READ|FA_WRITE);
        if (fresult != FR_OK){
            printf("Error No. %d in creating file %s.\r\n", fresult, name);
            return fresult;
        }else{
            printf("%s was created.\r\n", name);
        }

        fresult = f_close(&USBHFile);
        if (fresult != FR_OK){
            printf("Error No. %d in closing the file %s.\r\n", fresult, name);
        }
        else{
            printf("File %s was closed successfully.\r\n", name);
        }
    }
    return fresult;
}

FRESULT removeFile (char *name){
    fresult = f_stat (name, &USBHfno);
    if (fresult != FR_OK){
        printf("Error, %s does not exist.\r\n", name);
        return fresult;
    }else{
        fresult = f_unlink (name);
        if (fresult == FR_OK){
            printf("%s was removed successfully.\r\n", name);
        }else{
            printf("Error No. %d in removing %s.\r\n", fresult, name);
        }
    }
    return fresult;
}

FRESULT createDir (char *name){
    fresult = f_mkdir(name);
    if (fresult == FR_OK)
    {
        printf("%s was updated successfully.\r\n", name);
    }
    else
    {
        printf("Error No. %d in creating directory %s.\r\n", fresult, name);
    }
    return fresult;
}

FRESULT updateFile (char *name, char *data){
    fresult = f_stat (name, &USBHfno);
    if (fresult != FR_OK){
        printf("Error, %s does not exist...\r\n", name);
        return fresult;
    }else{
        fresult = f_open(&USBHFile, name, FA_OPEN_APPEND | FA_WRITE);
        if (fresult != FR_OK){
            printf("Error No. %d in opening file %s.\r\n", fresult, name);
            return fresult;
        }
        printf("Opening file %s to update the data.\r\n", name);

        fresult = f_write(&USBHFile, data, strlen (data), &bw);
        if (fresult != FR_OK){
            printf("Error No. %d in opening file %s.\r\n", fresult, name);
        }else{
            char *buf = malloc(100*sizeof(char));
            printf("%s updated successfully.\r\n", name);
        }
        fresult = f_close(&USBHFile);
        if (fresult != FR_OK){
            printf("Error No. %d in closing file %s.\r\n", fresult, name);
        }else{
            printf("File %s was closed successfully.\r\n", name);
        }
    }
    return fresult;
}
